using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    class Level {
        private string levelName;
        private uint level_ID;
        private uint time;
        private LevelTask task;

        public Level(string Name, uint ID, uint time, LevelTask task) { 
            this.levelName = Name;
            this.level_ID = ID;
            this.time = time;
            this.task = task;
        }

        public Level() { }
    }

}