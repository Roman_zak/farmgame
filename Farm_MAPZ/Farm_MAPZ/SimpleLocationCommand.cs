﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Farm_MAPZ
{
    internal class SimpleLocationCommand : ICommand
    {
        
        public Point execute(Point point)
        {
            Random random = new Random();
            int x = random.Next(Convert.ToInt32(new GameScene().ClientSize.Width * 0.1), Convert.ToInt32(new GameScene().ClientSize.Width * 0.9));
            //animalPic.Location = new Point(0, 0);
            int y = random.Next(Convert.ToInt32(new GameScene().ClientSize.Height * 0.25), Convert.ToInt32(new GameScene().ClientSize.Height * 0.75));
            return new Point(x, y);
        }
    }
}
