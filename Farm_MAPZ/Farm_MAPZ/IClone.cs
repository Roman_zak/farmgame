﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    /*    internal abstract class Clonnable
        {
            public abstract Clonnable clone();
        }*/
    internal interface IClone
    {
        IClone clone();
    }
}
