namespace Farm_MAPZ
{
    partial class GameScene
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameScene));
            this.labelMoney = new System.Windows.Forms.Label();
            this.labelChickens = new System.Windows.Forms.Label();
            this.labelSheeps = new System.Windows.Forms.Label();
            this.labelCows = new System.Windows.Forms.Label();
            this.labelWather = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.buttonCow = new System.Windows.Forms.Button();
            this.buttonWather = new System.Windows.Forms.Button();
            this.buttonSheep = new System.Windows.Forms.Button();
            this.buttonChicken = new System.Windows.Forms.Button();
            this.buttonShop = new System.Windows.Forms.Button();
            this.ManufactureButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMoney
            // 
            this.labelMoney.AutoSize = true;
            this.labelMoney.BackColor = System.Drawing.Color.Transparent;
            this.labelMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMoney.Location = new System.Drawing.Point(268, 86);
            this.labelMoney.Name = "labelMoney";
            this.labelMoney.Size = new System.Drawing.Size(152, 31);
            this.labelMoney.TabIndex = 2;
            this.labelMoney.Text = "labelMoney";
            // 
            // labelChickens
            // 
            this.labelChickens.AutoSize = true;
            this.labelChickens.BackColor = System.Drawing.Color.Transparent;
            this.labelChickens.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelChickens.Location = new System.Drawing.Point(605, 86);
            this.labelChickens.Name = "labelChickens";
            this.labelChickens.Size = new System.Drawing.Size(184, 31);
            this.labelChickens.TabIndex = 3;
            this.labelChickens.Text = "labelChickens";
            // 
            // labelSheeps
            // 
            this.labelSheeps.AutoSize = true;
            this.labelSheeps.BackColor = System.Drawing.Color.Transparent;
            this.labelSheeps.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSheeps.Location = new System.Drawing.Point(969, 86);
            this.labelSheeps.Name = "labelSheeps";
            this.labelSheeps.Size = new System.Drawing.Size(163, 31);
            this.labelSheeps.TabIndex = 4;
            this.labelSheeps.Text = "labelSheeps";
            // 
            // labelCows
            // 
            this.labelCows.AutoSize = true;
            this.labelCows.BackColor = System.Drawing.Color.Transparent;
            this.labelCows.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCows.Location = new System.Drawing.Point(1311, 86);
            this.labelCows.Name = "labelCows";
            this.labelCows.Size = new System.Drawing.Size(140, 31);
            this.labelCows.TabIndex = 5;
            this.labelCows.Text = "labelCows";
            // 
            // labelWather
            // 
            this.labelWather.AutoSize = true;
            this.labelWather.BackColor = System.Drawing.Color.Transparent;
            this.labelWather.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWather.Location = new System.Drawing.Point(1623, 86);
            this.labelWather.Name = "labelWather";
            this.labelWather.Size = new System.Drawing.Size(158, 31);
            this.labelWather.TabIndex = 6;
            this.labelWather.Text = "labelWather";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Image = global::Farm_MAPZ.Properties.Resources.SheepImage;
            this.pictureBox4.Location = new System.Drawing.Point(135, 65);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(85, 73);
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            // 
            // buttonCow
            // 
            this.buttonCow.BackColor = System.Drawing.Color.Transparent;
            this.buttonCow.BackgroundImage = global::Farm_MAPZ.Properties.Resources.CowImage;
            this.buttonCow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonCow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCow.Location = new System.Drawing.Point(1188, 62);
            this.buttonCow.Name = "buttonCow";
            this.buttonCow.Size = new System.Drawing.Size(95, 79);
            this.buttonCow.TabIndex = 11;
            this.buttonCow.UseVisualStyleBackColor = false;
            this.buttonCow.Click += new System.EventHandler(this.buttonCow_Click);
            // 
            // buttonWather
            // 
            this.buttonWather.BackColor = System.Drawing.Color.Transparent;
            this.buttonWather.BackgroundImage = global::Farm_MAPZ.Properties.Resources.WaterDropImage;
            this.buttonWather.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonWather.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonWather.Location = new System.Drawing.Point(1509, 67);
            this.buttonWather.Name = "buttonWather";
            this.buttonWather.Size = new System.Drawing.Size(79, 69);
            this.buttonWather.TabIndex = 12;
            this.buttonWather.UseVisualStyleBackColor = false;
            this.buttonWather.Click += new System.EventHandler(this.buttonWather_Click);
            // 
            // buttonSheep
            // 
            this.buttonSheep.BackColor = System.Drawing.Color.Transparent;
            this.buttonSheep.BackgroundImage = global::Farm_MAPZ.Properties.Resources.SheepImage;
            this.buttonSheep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSheep.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSheep.Location = new System.Drawing.Point(850, 64);
            this.buttonSheep.Name = "buttonSheep";
            this.buttonSheep.Size = new System.Drawing.Size(87, 75);
            this.buttonSheep.TabIndex = 13;
            this.buttonSheep.UseVisualStyleBackColor = false;
            this.buttonSheep.Click += new System.EventHandler(this.buttonSheep_Click);
            // 
            // buttonChicken
            // 
            this.buttonChicken.BackColor = System.Drawing.Color.Transparent;
            this.buttonChicken.BackgroundImage = global::Farm_MAPZ.Properties.Resources.ChikenImage;
            this.buttonChicken.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonChicken.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonChicken.Location = new System.Drawing.Point(473, 62);
            this.buttonChicken.Name = "buttonChicken";
            this.buttonChicken.Size = new System.Drawing.Size(78, 78);
            this.buttonChicken.TabIndex = 14;
            this.buttonChicken.UseVisualStyleBackColor = false;
            this.buttonChicken.Click += new System.EventHandler(this.buttonChicken_Click);
            // 
            // buttonShop
            // 
            this.buttonShop.BackColor = System.Drawing.Color.Transparent;
            this.buttonShop.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Store;
            this.buttonShop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonShop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonShop.Location = new System.Drawing.Point(1687, 841);
            this.buttonShop.Name = "buttonShop";
            this.buttonShop.Size = new System.Drawing.Size(106, 85);
            this.buttonShop.TabIndex = 15;
            this.buttonShop.UseVisualStyleBackColor = false;
            this.buttonShop.Click += new System.EventHandler(this.buttonShop_Click);
            // 
            // ManufactureButton
            // 
            this.ManufactureButton.BackColor = System.Drawing.Color.Transparent;
            this.ManufactureButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ManufactureButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ManufactureButton.Location = new System.Drawing.Point(15, 285);
            this.ManufactureButton.Name = "ManufactureButton";
            this.ManufactureButton.Size = new System.Drawing.Size(205, 196);
            this.ManufactureButton.TabIndex = 16;
            this.ManufactureButton.UseVisualStyleBackColor = false;
            this.ManufactureButton.Click += new System.EventHandler(this.ManufactureButton_Click);
            // 
            // GameScene
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1902, 1033);
            this.Controls.Add(this.ManufactureButton);
            this.Controls.Add(this.buttonShop);
            this.Controls.Add(this.buttonChicken);
            this.Controls.Add(this.buttonSheep);
            this.Controls.Add(this.buttonWather);
            this.Controls.Add(this.buttonCow);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.labelWather);
            this.Controls.Add(this.labelCows);
            this.Controls.Add(this.labelSheeps);
            this.Controls.Add(this.labelChickens);
            this.Controls.Add(this.labelMoney);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Name = "GameScene";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameScene";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.GameScene_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelMoney;
        private System.Windows.Forms.Label labelChickens;
        private System.Windows.Forms.Label labelSheeps;
        private System.Windows.Forms.Label labelCows;
        private System.Windows.Forms.Label labelWather;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button buttonCow;
        private System.Windows.Forms.Button buttonWather;
        private System.Windows.Forms.Button buttonSheep;
        private System.Windows.Forms.Button buttonChicken;
        private System.Windows.Forms.Button buttonShop;
        private System.Windows.Forms.Button ManufactureButton;
    }
}
