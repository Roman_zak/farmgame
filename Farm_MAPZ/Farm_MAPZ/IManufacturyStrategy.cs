﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Farm_MAPZ
{
    internal interface IManufacturyStrategy
    {
        Product manufacture();

        PictureBox GetPictureBox();
    }
}
