﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class LevelFactoryProxy : ILevelFactory
    {
        private ILevelFactory _concreteLevelFactory;
        private string role;

        public LevelFactoryProxy(ILevelFactory concreteLevelFactory, string role)
        {
            _concreteLevelFactory = concreteLevelFactory;
            this.role = role;
        }

        public Level CreateLevel()
        {
            Level level = _concreteLevelFactory.CreateLevel();
            switch (role)
            {
                case "Human":
                    Economy.GetEconomy().Money*=1;
                    break;
                case "Elf":
                    Economy.GetEconomy().Money *= 2;
                    break;
                case "Dworf":
                    Economy.GetEconomy().Money *= 10;
                    break;
                default:
                    break;
            }
            return level;
        }
    }
}
