﻿namespace Farm_MAPZ
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLevelEz = new System.Windows.Forms.Button();
            this.buttonLevelNormal = new System.Windows.Forms.Button();
            this.buttonLevelHard = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonLevelEz
            // 
            this.buttonLevelEz.BackColor = System.Drawing.Color.Transparent;
            this.buttonLevelEz.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Level31;
            this.buttonLevelEz.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonLevelEz.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLevelEz.Location = new System.Drawing.Point(309, 79);
            this.buttonLevelEz.Name = "buttonLevelEz";
            this.buttonLevelEz.Size = new System.Drawing.Size(150, 75);
            this.buttonLevelEz.TabIndex = 0;
            this.buttonLevelEz.UseVisualStyleBackColor = false;
            this.buttonLevelEz.Click += new System.EventHandler(this.buttonLevelEz_Click);
            // 
            // buttonLevelNormal
            // 
            this.buttonLevelNormal.BackColor = System.Drawing.Color.Transparent;
            this.buttonLevelNormal.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Level1;
            this.buttonLevelNormal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonLevelNormal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLevelNormal.Location = new System.Drawing.Point(309, 170);
            this.buttonLevelNormal.Name = "buttonLevelNormal";
            this.buttonLevelNormal.Size = new System.Drawing.Size(150, 75);
            this.buttonLevelNormal.TabIndex = 1;
            this.buttonLevelNormal.UseVisualStyleBackColor = false;
            this.buttonLevelNormal.Click += new System.EventHandler(this.buttonLevelNormal_Click);
            // 
            // buttonLevelHard
            // 
            this.buttonLevelHard.BackColor = System.Drawing.Color.Transparent;
            this.buttonLevelHard.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Level2;
            this.buttonLevelHard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonLevelHard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLevelHard.Location = new System.Drawing.Point(309, 263);
            this.buttonLevelHard.Name = "buttonLevelHard";
            this.buttonLevelHard.Size = new System.Drawing.Size(150, 75);
            this.buttonLevelHard.TabIndex = 2;
            this.buttonLevelHard.UseVisualStyleBackColor = false;
            this.buttonLevelHard.Click += new System.EventHandler(this.buttonLevelHard_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Farm_MAPZ.Properties.Resources.settings1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonLevelHard);
            this.Controls.Add(this.buttonLevelNormal);
            this.Controls.Add(this.buttonLevelEz);
            this.Name = "Start";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Start";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonLevelEz;
        private System.Windows.Forms.Button buttonLevelNormal;
        private System.Windows.Forms.Button buttonLevelHard;
    }
}