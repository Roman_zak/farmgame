﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_MAPZ
{
    public partial class GameScene : Form
    {
        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
        public GameScene()
        {
            
            InitializeComponent();
            player.SoundLocation = "Techno Chicken.wav";
        }
        int ChickenCount() {
            Economy economy = Economy.GetEconomy();
            return economy.Animals.Count(animal => animal.AnimalType == AnimalType.Chicken);
        }

        int SheepCount()
        {
            Economy economy = Economy.GetEconomy();
            return economy.Animals.Count(animal => animal.AnimalType == AnimalType.Sheep);
        }

        int CowCount()
        {
            Economy economy = Economy.GetEconomy();
            return economy.Animals.Count(animal => animal.AnimalType == AnimalType.Cow);
        }

        public void UpdateEconomy() {
            Economy economy = Economy.GetEconomy();
            this.labelMoney.Text = economy.Money.ToString();
            this.labelWather.Text = economy._well.Volume.ToString();
            this.labelChickens.Text = ChickenCount().ToString();
            this.labelCows.Text = CowCount().ToString();
            this.labelSheeps.Text = SheepCount().ToString();
        }
        void UpdateAnimals()
        {
            Invoker invoker = new Invoker();
            invoker.SetOnStart(new SimpleLocationCommand());

            List<Animal> animals = Economy.GetEconomy().Animals;
            animals.ForEach(animal => {
                animal.animalPic.Left = new Random().Next(Convert.ToInt32(new GameScene().ClientSize.Width * 0.2), Convert.ToInt32(new GameScene().ClientSize.Width * 0.7));
                //animalPic.Location = new Point(0, 0);
                animal.animalPic.Top = new Random().Next(Convert.ToInt32(new GameScene().ClientSize.Height * 0.2), Convert.ToInt32(new GameScene().ClientSize.Height * 0.6));
                this.Controls.Add(animal.animalPic);
                animal.makeProductive(this);
            });
        }
        

        private void GameScene_Load(object sender, EventArgs e)
        {
            UpdateEconomy();
            UpdateAnimals();
            player.PlayLooping();
            this.ManufactureButton.BackgroundImage = Economy.GetEconomy().manufacturyContext.GetStrategy().GetPictureBox().Image;
        }

        private void buttonChicken_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            Animal chicken = AnimalRepository.GetAnimalRepository().Chicken;
            if (economy.Money < chicken.PriceToBuy)
            {

            }
            else
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleLocationCommand());
                invoker.SetOnFinish(new ComplexLocationCommand(Point.Empty, this, AnimalRepository.GetAnimalRepository().Chicken));
                invoker.DoSomethingImportant();
                /*                economy.Money -= chicken.PriceToBuy;
                                Animal chickenClone = (Animal)chicken.clone();
                                economy.Animals.Add(chickenClone);
                                this.Controls.Add(chickenClone.animalPic);
                                chickenClone.makeProductive(this);*/
                UpdateEconomy();
            }
        }

        private void buttonSheep_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            Animal sheep = AnimalRepository.GetAnimalRepository().Sheep;
            if (economy.Money < sheep.PriceToBuy)
            {

            }
            else
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleLocationCommand());
                invoker.SetOnFinish(new ComplexLocationCommand(Point.Empty, this, AnimalRepository.GetAnimalRepository().Sheep));
                invoker.DoSomethingImportant();
                /*                economy.Money -= sheep.PriceToBuy;
                                Animal sheepClone = (Animal)sheep.clone();
                                sheepClone.makeProductive(this);
                                economy.Animals.Add(sheepClone);
                                this.Controls.Add(sheepClone.animalPic);*/
                UpdateEconomy();
            }
        }

        private void buttonCow_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            Animal cow = AnimalRepository.GetAnimalRepository().Cow;
            if (economy.Money < cow.PriceToBuy)
            {

            }
            else
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleLocationCommand());
                invoker.SetOnFinish(new ComplexLocationCommand(Point.Empty, this, AnimalRepository.GetAnimalRepository().Cow));
                invoker.DoSomethingImportant();
                /*                economy.Money -= cow.PriceToBuy;
                                Animal cowClone = (Animal)cow.clone();
                                cowClone.makeProductive(this);
                                economy.Animals.Add(cowClone);
                                this.Controls.Add(cowClone.animalPic);*/

                UpdateEconomy();
            }
        }

        private void buttonShop_Click(object sender, EventArgs e)
        {
            Shop shop  = new Shop(this);
            shop.ShowDialog();
        }

        private void buttonWather_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            if (economy._well.pullWater()) { 
                   
            }
        }

        private void ManufactureButton_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            economy.manufacturyContext.DoBusiness();
        }
    }
}
