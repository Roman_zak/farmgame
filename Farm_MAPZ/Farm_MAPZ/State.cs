﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal abstract class State
    {
        protected Animal _context;

        public void SetContext(Animal context)
        {
            this._context = context;
        }

        public abstract void HandleProduce();
    }
}
