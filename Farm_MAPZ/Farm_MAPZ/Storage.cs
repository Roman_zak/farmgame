﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Farm_MAPZ
{
    internal class Storage
    {
        public  uint Space { get; set; }

        public  uint ContentVolume { get; set; }

        public Storage(uint space)
        {
            this.Space = space;
           // this.Products = new List<Product>();
        }
        //public  List<Product> Products { get; set; }
        public Dictionary<ProductType, ProductShelf> ProductShelfs = new Dictionary<ProductType, ProductShelf>();

        public uint storageWorth()
        {
            uint sum = 0;
            foreach (KeyValuePair<ProductType, ProductShelf> shelf in ProductShelfs)
            {
                // do something with entry.Value or entry.Key
                sum+=shelf.Value.Operation();
            }
            return sum;
        }
        public bool isFull()
        {
            return ContentVolume == Space;
        }
        public  bool willFit(Product prod)
        {
            return ContentVolume + prod.Capacity <= Space;
        }

        public void addProduct(Product prod)
        {
            ProductShelf shelf = new ProductShelf(prod.ProductType);
            if (willFit(prod)){
                if(ProductShelfs.TryGetValue(prod.ProductType, out shelf))
                {
                    Console.WriteLine(prod.ProductType.ToString() + "had shelf and added");
                    shelf.Add(prod);
                }
                else
                {
                    Console.WriteLine(prod.ProductType.ToString() + "had not shelf ");
                    ProductShelfs.Add(prod.ProductType, new ProductShelf(prod.ProductType));
                    ProductShelfs.TryGetValue(prod.ProductType, out shelf);
                    shelf.Add(prod);
                    Console.WriteLine(prod.ProductType.ToString() + " shelf created and added");
                }
                ContentVolume += prod.Capacity;
            }
        }

        public bool removeProduct(ProductType productType)
        {
            ProductShelf shelf = new ProductShelf(productType);
            if(ProductShelfs.TryGetValue(productType, out shelf))
            {
                if (shelf.getChildrens().Any())
                {
                    shelf.Remove(shelf.ProductType);
                    switch (productType)
                    {
                        case ProductType.Egg:
                            ContentVolume -= ProductRepository.GetProductRepository().Egg.Capacity; break;
                        case ProductType.Wool:
                            ContentVolume -= ProductRepository.GetProductRepository().Wool.Capacity; break;
                        case ProductType.Milk:
                            ContentVolume -= ProductRepository.GetProductRepository().Milk.Capacity; break;
                        default:
                            ContentVolume -= ProductRepository.GetProductRepository().Egg.Capacity; break;
                    }
                    return true;
                }
                else
                {
                    return false;
                }

                
            }
            return false;
        }
    }
}
