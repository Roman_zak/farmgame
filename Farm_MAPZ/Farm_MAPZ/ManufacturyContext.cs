﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class ManufacturyContext
    {
        private IManufacturyStrategy _strategy;
        public ManufacturyContext()
        { }
        public ManufacturyContext(IManufacturyStrategy strategy)
        {
            this._strategy = strategy;
        }
        public void SetStrategy(IManufacturyStrategy strategy)
        {
            this._strategy = strategy;
        }
        public IManufacturyStrategy GetStrategy()
        {
            return this._strategy; 
        }
        public void DoBusiness()
        {
            this._strategy.manufacture();
        }
    }
}
