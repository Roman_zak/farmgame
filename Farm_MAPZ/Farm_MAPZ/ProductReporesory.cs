﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class ProductRepository
    {
        private static ProductRepository _productRepository;
        private ProductRepository()
        {
            Egg = new Product(price: 10, capacity: 1, type: ProductType.Egg);
            Flour = new Product(price: 20, capacity: 2, type: ProductType.Flour);
            Pancakes = new Product(price: 40, capacity: 3, type: ProductType.Pancakes);

            Wool = new Product(price: 100, capacity: 3, type: ProductType.Wool);
            Clew = new Product(price: 200, capacity: 3, type: ProductType.Clew);
            Sweater = new Product(price: 400, capacity: 3, type: ProductType.Sweater);

            Milk = new Product(price: 1000, capacity: 5, type: ProductType.Milk);
            Cream = new Product(price: 2000, capacity: 5, type: ProductType.Cream);
            Cheese = new Product(price: 4000, capacity: 5, type: ProductType.Cheese);
        }
        public static ProductRepository GetProductRepository()
        {
            if (_productRepository == null)
            {
                _productRepository = new ProductRepository();
            }
            return _productRepository;
        }
        public Product Egg { get; set; }
        public Product Flour { get; set; }
        public Product Pancakes { get; set; }

        public Product Wool { get; set; }
        public Product Clew { get; set; }
        public Product Sweater { get; set; }

        public Product Milk { get; set; }
        public Product Cream { get; set; }
        public Product Cheese { get; set; }


    }

}