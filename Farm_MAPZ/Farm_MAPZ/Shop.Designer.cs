﻿namespace Farm_MAPZ
{
    partial class Shop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.buttonAddChicken = new System.Windows.Forms.Button();
            this.buttonRemoveChicken = new System.Windows.Forms.Button();
            this.buttonAddSheep = new System.Windows.Forms.Button();
            this.buttonRemoveSheep = new System.Windows.Forms.Button();
            this.buttonAddCow = new System.Windows.Forms.Button();
            this.buttonRemoveCow = new System.Windows.Forms.Button();
            this.labelChickenCount = new System.Windows.Forms.Label();
            this.labelSheepCount = new System.Windows.Forms.Label();
            this.labelCowCount = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelMilkCount = new System.Windows.Forms.Label();
            this.labelWoolCount = new System.Windows.Forms.Label();
            this.labelEggCount = new System.Windows.Forms.Label();
            this.label_StorageWorth = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label_Eggs = new System.Windows.Forms.Label();
            this.label_Nytka = new System.Windows.Forms.Label();
            this.label_Cream = new System.Windows.Forms.Label();
            this.label_CreamSell = new System.Windows.Forms.Label();
            this.label_NytkaSell = new System.Windows.Forms.Label();
            this.label_FlourSell = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Farm_MAPZ.Properties.Resources.ChikenImage;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(40, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::Farm_MAPZ.Properties.Resources.SheepImage;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(344, 43);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 100);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Farm_MAPZ.Properties.Resources.CowImage;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(648, 43);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 100);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // buttonAddChicken
            // 
            this.buttonAddChicken.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAddChicken.Location = new System.Drawing.Point(40, 149);
            this.buttonAddChicken.Name = "buttonAddChicken";
            this.buttonAddChicken.Size = new System.Drawing.Size(30, 30);
            this.buttonAddChicken.TabIndex = 3;
            this.buttonAddChicken.Text = "+";
            this.buttonAddChicken.UseVisualStyleBackColor = true;
            this.buttonAddChicken.Click += new System.EventHandler(this.buttonAddChicken_Click);
            // 
            // buttonRemoveChicken
            // 
            this.buttonRemoveChicken.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRemoveChicken.Location = new System.Drawing.Point(110, 149);
            this.buttonRemoveChicken.Name = "buttonRemoveChicken";
            this.buttonRemoveChicken.Size = new System.Drawing.Size(30, 30);
            this.buttonRemoveChicken.TabIndex = 4;
            this.buttonRemoveChicken.Text = "-";
            this.buttonRemoveChicken.UseVisualStyleBackColor = true;
            this.buttonRemoveChicken.Click += new System.EventHandler(this.buttonRemoveChicken_Click);
            // 
            // buttonAddSheep
            // 
            this.buttonAddSheep.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAddSheep.Location = new System.Drawing.Point(344, 149);
            this.buttonAddSheep.Name = "buttonAddSheep";
            this.buttonAddSheep.Size = new System.Drawing.Size(30, 30);
            this.buttonAddSheep.TabIndex = 5;
            this.buttonAddSheep.Text = "+";
            this.buttonAddSheep.UseVisualStyleBackColor = true;
            this.buttonAddSheep.Click += new System.EventHandler(this.buttonAddSheep_Click);
            // 
            // buttonRemoveSheep
            // 
            this.buttonRemoveSheep.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRemoveSheep.Location = new System.Drawing.Point(414, 149);
            this.buttonRemoveSheep.Name = "buttonRemoveSheep";
            this.buttonRemoveSheep.Size = new System.Drawing.Size(30, 30);
            this.buttonRemoveSheep.TabIndex = 6;
            this.buttonRemoveSheep.Text = "-";
            this.buttonRemoveSheep.UseVisualStyleBackColor = true;
            this.buttonRemoveSheep.Click += new System.EventHandler(this.buttonRemoveSheep_Click);
            // 
            // buttonAddCow
            // 
            this.buttonAddCow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAddCow.Location = new System.Drawing.Point(648, 149);
            this.buttonAddCow.Name = "buttonAddCow";
            this.buttonAddCow.Size = new System.Drawing.Size(30, 30);
            this.buttonAddCow.TabIndex = 7;
            this.buttonAddCow.Text = "+";
            this.buttonAddCow.UseVisualStyleBackColor = true;
            this.buttonAddCow.Click += new System.EventHandler(this.buttonAddCow_Click);
            // 
            // buttonRemoveCow
            // 
            this.buttonRemoveCow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRemoveCow.Location = new System.Drawing.Point(718, 149);
            this.buttonRemoveCow.Name = "buttonRemoveCow";
            this.buttonRemoveCow.Size = new System.Drawing.Size(30, 30);
            this.buttonRemoveCow.TabIndex = 8;
            this.buttonRemoveCow.Text = "-";
            this.buttonRemoveCow.UseVisualStyleBackColor = true;
            this.buttonRemoveCow.Click += new System.EventHandler(this.buttonRemoveCow_Click);
            // 
            // labelChickenCount
            // 
            this.labelChickenCount.AutoSize = true;
            this.labelChickenCount.Location = new System.Drawing.Point(74, 149);
            this.labelChickenCount.MaximumSize = new System.Drawing.Size(30, 30);
            this.labelChickenCount.MinimumSize = new System.Drawing.Size(30, 30);
            this.labelChickenCount.Name = "labelChickenCount";
            this.labelChickenCount.Size = new System.Drawing.Size(30, 30);
            this.labelChickenCount.TabIndex = 9;
            // 
            // labelSheepCount
            // 
            this.labelSheepCount.AutoSize = true;
            this.labelSheepCount.Location = new System.Drawing.Point(378, 149);
            this.labelSheepCount.MaximumSize = new System.Drawing.Size(30, 30);
            this.labelSheepCount.MinimumSize = new System.Drawing.Size(30, 30);
            this.labelSheepCount.Name = "labelSheepCount";
            this.labelSheepCount.Size = new System.Drawing.Size(30, 30);
            this.labelSheepCount.TabIndex = 10;
            // 
            // labelCowCount
            // 
            this.labelCowCount.AutoSize = true;
            this.labelCowCount.Location = new System.Drawing.Point(682, 149);
            this.labelCowCount.MaximumSize = new System.Drawing.Size(30, 30);
            this.labelCowCount.MinimumSize = new System.Drawing.Size(30, 30);
            this.labelCowCount.Name = "labelCowCount";
            this.labelCowCount.Size = new System.Drawing.Size(30, 30);
            this.labelCowCount.TabIndex = 11;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Eg;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(40, 239);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 120);
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Wool;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(344, 239);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 120);
            this.pictureBox5.TabIndex = 13;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Milk;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(648, 239);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(100, 120);
            this.pictureBox6.TabIndex = 14;
            this.pictureBox6.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 372);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "10";
            this.label1.Click += new System.EventHandler(this.labelEgg_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(378, 372);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "100";
            this.label2.Click += new System.EventHandler(this.labelWool_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(682, 372);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "1000";
            this.label3.Click += new System.EventHandler(this.labelMilk_Click);
            // 
            // labelMilkCount
            // 
            this.labelMilkCount.AutoSize = true;
            this.labelMilkCount.Location = new System.Drawing.Point(682, 209);
            this.labelMilkCount.Name = "labelMilkCount";
            this.labelMilkCount.Size = new System.Drawing.Size(35, 16);
            this.labelMilkCount.TabIndex = 20;
            this.labelMilkCount.Text = "1000";
            // 
            // labelWoolCount
            // 
            this.labelWoolCount.AutoSize = true;
            this.labelWoolCount.Location = new System.Drawing.Point(378, 209);
            this.labelWoolCount.Name = "labelWoolCount";
            this.labelWoolCount.Size = new System.Drawing.Size(28, 16);
            this.labelWoolCount.TabIndex = 19;
            this.labelWoolCount.Text = "100";
            // 
            // labelEggCount
            // 
            this.labelEggCount.AutoSize = true;
            this.labelEggCount.Location = new System.Drawing.Point(74, 209);
            this.labelEggCount.Name = "labelEggCount";
            this.labelEggCount.Size = new System.Drawing.Size(21, 16);
            this.labelEggCount.TabIndex = 18;
            this.labelEggCount.Text = "10";
            this.labelEggCount.Click += new System.EventHandler(this.labelEggCount_Click);
            // 
            // label_StorageWorth
            // 
            this.label_StorageWorth.AutoSize = true;
            this.label_StorageWorth.BackColor = System.Drawing.Color.Transparent;
            this.label_StorageWorth.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label_StorageWorth.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_StorageWorth.ForeColor = System.Drawing.Color.White;
            this.label_StorageWorth.Location = new System.Drawing.Point(12, 9);
            this.label_StorageWorth.Name = "label_StorageWorth";
            this.label_StorageWorth.Size = new System.Drawing.Size(153, 26);
            this.label_StorageWorth.TabIndex = 21;
            this.label_StorageWorth.Text = "Storage worth:";
            this.label_StorageWorth.Click += new System.EventHandler(this.label4_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox7.BackgroundImage = global::Farm_MAPZ.Properties.Resources.eggs;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(40, 455);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 120);
            this.pictureBox7.TabIndex = 12;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Nitka;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(344, 455);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 120);
            this.pictureBox8.TabIndex = 13;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.BackgroundImage = global::Farm_MAPZ.Properties.Resources.yogurt;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(648, 455);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 120);
            this.pictureBox9.TabIndex = 14;
            this.pictureBox9.TabStop = false;
            // 
            // label_Eggs
            // 
            this.label_Eggs.AutoSize = true;
            this.label_Eggs.Location = new System.Drawing.Point(74, 425);
            this.label_Eggs.Name = "label_Eggs";
            this.label_Eggs.Size = new System.Drawing.Size(21, 16);
            this.label_Eggs.TabIndex = 18;
            this.label_Eggs.Text = "20";
            this.label_Eggs.Click += new System.EventHandler(this.labelEggCount_Click);
            // 
            // label_Nytka
            // 
            this.label_Nytka.AutoSize = true;
            this.label_Nytka.Location = new System.Drawing.Point(378, 425);
            this.label_Nytka.Name = "label_Nytka";
            this.label_Nytka.Size = new System.Drawing.Size(28, 16);
            this.label_Nytka.TabIndex = 19;
            this.label_Nytka.Text = "200";
            // 
            // label_Cream
            // 
            this.label_Cream.AutoSize = true;
            this.label_Cream.Location = new System.Drawing.Point(682, 425);
            this.label_Cream.Name = "label_Cream";
            this.label_Cream.Size = new System.Drawing.Size(35, 16);
            this.label_Cream.TabIndex = 20;
            this.label_Cream.Text = "2000";
            // 
            // label_CreamSell
            // 
            this.label_CreamSell.AutoSize = true;
            this.label_CreamSell.Location = new System.Drawing.Point(677, 608);
            this.label_CreamSell.Name = "label_CreamSell";
            this.label_CreamSell.Size = new System.Drawing.Size(35, 16);
            this.label_CreamSell.TabIndex = 22;
            this.label_CreamSell.Text = "2000";
            this.label_CreamSell.Click += new System.EventHandler(this.label_CreamSell_Click);
            // 
            // label_NytkaSell
            // 
            this.label_NytkaSell.AutoSize = true;
            this.label_NytkaSell.Location = new System.Drawing.Point(381, 608);
            this.label_NytkaSell.Name = "label_NytkaSell";
            this.label_NytkaSell.Size = new System.Drawing.Size(28, 16);
            this.label_NytkaSell.TabIndex = 23;
            this.label_NytkaSell.Text = "200";
            this.label_NytkaSell.Click += new System.EventHandler(this.label_NytkaSell_Click);
            // 
            // label_FlourSell
            // 
            this.label_FlourSell.AutoSize = true;
            this.label_FlourSell.Location = new System.Drawing.Point(67, 605);
            this.label_FlourSell.Name = "label_FlourSell";
            this.label_FlourSell.Size = new System.Drawing.Size(21, 16);
            this.label_FlourSell.TabIndex = 24;
            this.label_FlourSell.Text = "20";
            this.label_FlourSell.Click += new System.EventHandler(this.label_FlourSell_Click);
            // 
            // Shop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Farm_MAPZ.Properties.Resources.ShopBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(821, 681);
            this.Controls.Add(this.label_FlourSell);
            this.Controls.Add(this.label_NytkaSell);
            this.Controls.Add(this.label_CreamSell);
            this.Controls.Add(this.label_StorageWorth);
            this.Controls.Add(this.label_Cream);
            this.Controls.Add(this.labelMilkCount);
            this.Controls.Add(this.label_Nytka);
            this.Controls.Add(this.labelWoolCount);
            this.Controls.Add(this.label_Eggs);
            this.Controls.Add(this.labelEggCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.labelCowCount);
            this.Controls.Add(this.labelSheepCount);
            this.Controls.Add(this.labelChickenCount);
            this.Controls.Add(this.buttonRemoveCow);
            this.Controls.Add(this.buttonAddCow);
            this.Controls.Add(this.buttonRemoveSheep);
            this.Controls.Add(this.buttonAddSheep);
            this.Controls.Add(this.buttonRemoveChicken);
            this.Controls.Add(this.buttonAddChicken);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Name = "Shop";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shop";
            this.Load += new System.EventHandler(this.Shop_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button buttonAddChicken;
        private System.Windows.Forms.Button buttonRemoveChicken;
        private System.Windows.Forms.Button buttonAddSheep;
        private System.Windows.Forms.Button buttonRemoveSheep;
        private System.Windows.Forms.Button buttonAddCow;
        private System.Windows.Forms.Button buttonRemoveCow;
        private System.Windows.Forms.Label labelChickenCount;
        private System.Windows.Forms.Label labelSheepCount;
        private System.Windows.Forms.Label labelCowCount;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelMilkCount;
        private System.Windows.Forms.Label labelWoolCount;
        private System.Windows.Forms.Label labelEggCount;
        private System.Windows.Forms.Label label_StorageWorth;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label_Eggs;
        private System.Windows.Forms.Label label_Nytka;
        private System.Windows.Forms.Label label_Cream;
        private System.Windows.Forms.Label label_CreamSell;
        private System.Windows.Forms.Label label_NytkaSell;
        private System.Windows.Forms.Label label_FlourSell;
    }
}