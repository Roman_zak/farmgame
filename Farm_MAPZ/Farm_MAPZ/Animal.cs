﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using  System.Windows.Forms;

namespace Farm_MAPZ {
    internal class Animal : IClone
    {
        Random random = new Random();
        private State state;
        public Animal(State state, AnimalType animalType, uint priceToBuy, Product productToProduce)
        {
            this.state = state;
            //state.SetContext(this);
            this.AnimalType = animalType;
            this.PriceToBuy = priceToBuy;
            this.ProductToProduce = productToProduce;
           
        }
        public void TransitionTo(State state)
        {
            Console.WriteLine($"Context: Transition to {state.GetType().Name}.");
            this.state = state;
            this.state.SetContext(this);
        }

        // The Context delegates part of its behavior to the current State
        // object.
        public void RequestProduce()
        {
            this.state.HandleProduce();
        }
        public AnimalType AnimalType { get; }
        public uint PriceToBuy { get; set; }
        public Product ProductToProduce { get; set; }
        static public uint MaxHealth { get; set; }
        public uint Health { get; set; }

        public PictureBox animalPic;

        public Product produceProduct()
        {
            Console.WriteLine(this.ProductToProduce.ToString() + " created");
            if (Economy.GetEconomy().Storage.willFit(ProductToProduce))
            {
                Product prod = (Product)ProductToProduce.clone();
                Economy.GetEconomy().Storage.addProduct(prod);
                this.RequestProduce();
                return prod;
            }
            else
            {
                return null;
            }

        }
        public void Eat()
        {
            //if(general food == 0){
            // 
            //}
            Health++;
            Well.grass--;
            // general food--;
        }
        public void Die()
        {

        }

        static TimeSpan startTimeSpan = TimeSpan.Zero;
        static TimeSpan periodTimeSpan = TimeSpan.FromSeconds(10);
        public void makeProductive(Form form)
        {
            var timer = new System.Threading.Timer((e) =>
            {
                Product prod = produceProduct();
/*                form.Controls.Add(prod.productPic);
                Console.WriteLine(form.ToString());*/
            }, null, startTimeSpan, periodTimeSpan);
        }


        public IClone clone()
        {
            Animal copy = (Animal)this.MemberwiseClone();
            copy.ProductToProduce = (Product)this.ProductToProduce.clone();
            copy.animalPic = new PictureBox();
            switch (copy.AnimalType)
            {
                case AnimalType.Chicken:
                    copy.animalPic.Image = Properties.Resources.ChikenImage; break;
                case AnimalType.Sheep:
                    copy.animalPic.Image = Properties.Resources.SheepImage; break;
                case AnimalType.Cow:
                    copy.animalPic.Image = Properties.Resources.CowImage; break;
                default: copy.animalPic.Image = Properties.Resources.ChikenImage; break;
            }
            copy.animalPic.SizeMode = PictureBoxSizeMode.StretchImage;
            copy.animalPic.Height = 80;
            copy.animalPic.Width = 60;
            copy.animalPic.BackColor = System.Drawing.Color.Transparent;

            /*copy.animalPic.Left = random.Next(Convert.ToInt32(new GameScene().ClientSize.Width * 0.2), Convert.ToInt32(new GameScene().ClientSize.Width * 0.7));
            //animalPic.Location = new Point(0, 0);
            copy.animalPic.Top = random.Next(Convert.ToInt32(new GameScene().ClientSize.Height * 0.2), Convert.ToInt32(new GameScene().ClientSize.Height * 0.6));*/
            
            
            copy.state.SetContext(copy);
            Console.WriteLine(copy.animalPic.Location);
            
            return copy;
        }

        public override string ToString()
        {
            return AnimalType.ToString() + " price:" + PriceToBuy.ToString();
        }
    }
    internal enum AnimalType
    {
        Chicken,
        Sheep,
        Cow
    }

}