﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class FacadeUpdate
    {
        protected GameScene _scene;

        protected Shop _shop;

        public FacadeUpdate(GameScene scene, Shop shop) { 
            _scene = scene;
            _shop = shop;
        }
        public void Update() { 
            _scene.UpdateEconomy();
            _shop.UpdateShop();
            Economy.GetEconomy().Info();
        }
    }
}
