﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal sealed class Economy
    {
        public Well _well;

        private static Economy _economy;
        public uint Money { get; set; }
        public List<Animal> Animals { get; set; }
        public Storage Storage { get; set; }
        public ManufacturyContext manufacturyContext { get; set; }
        public static Economy GetEconomy()
        {
            if (_economy == null)
            {
                _economy = new Economy();
            }
            return _economy;
        }
        private Economy()
        {
            //Buildings = new List<Building>();
            Animals = new List<Animal>();
            _well = new Well();
            _well.Volume = 10;
        }
        public bool removeAnimal(AnimalType animalType)
        {
            return Animals.Remove(Animals.Find(animal => animal.AnimalType == animalType));
        }
        public void Info()
        {
            Console.WriteLine("Economy stats: \n" +
                "Money: {0}\n" +
                "Animals Count: {1}\n" +
                "Storage: {2}\n" +
                "Storage worth: {3}\n", _economy.Money, _economy.Animals.Count, _economy.Storage.ContentVolume,  _economy.Storage.storageWorth());
        }

        /*        public Economy(uint money, List<Animal> _animals, Storage _storage*//*, List<Buildings> _buildings*//*) { 
                    this.Money = money;
                    this.animals = _animals;
                    this.storage = _storage;
                }*/
    }
}
