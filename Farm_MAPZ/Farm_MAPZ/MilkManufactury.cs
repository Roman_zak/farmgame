﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Farm_MAPZ
{
    internal class MilkManufactury : IManufacturyStrategy
    {
        public Product ProductIn = ProductRepository.GetProductRepository().Milk;
        public Product ProductOut = ProductRepository.GetProductRepository().Cream;
        private PictureBox manufactPic = new PictureBox();
        public MilkManufactury()
        {
            manufactPic.Image = Properties.Resources.MilkFarm;
        }
        public PictureBox GetPictureBox()
        {
            return manufactPic;
        }
        public Product manufacture()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductIn.ProductType);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductIn.ProductType, out shelf))
            {
                shelf.Remove(ProductIn.ProductType);
                Product newProd = (Product)ProductOut.clone();
                economy.Storage.addProduct(newProd);
                return newProd;
            }
            else
            {
                return null;
            }
        }
    }
}
