﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class ChickenState : State
    {
        bool _isLeft = true;
        public override void HandleProduce()
        {
            Console.WriteLine("Chicken produce");
            if (_isLeft)
            {
                this._context.animalPic.Image = Properties.Resources.ChikenImage_right;
                _isLeft = false;
            }
            else
            {
                this._context.animalPic.Image = Properties.Resources.ChikenImage;
                _isLeft = true;
            }
            
        }

    }
}
