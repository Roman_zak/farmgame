﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_MAPZ
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioButtonHuman_CheckedChanged(object sender, EventArgs e)
        {
            Form1.Role = "Human";
        }

        private void radioButtonDworf_CheckedChanged(object sender, EventArgs e)
        {
            Form1.Role = "Dworf";
        }

        private void radioButtonElf_CheckedChanged(object sender, EventArgs e)
        {
            Form1.Role = "Elf";
        }
    }
}
