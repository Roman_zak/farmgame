﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Farm_MAPZ
{
    interface ICommand
    {
        Point execute(Point point);
    }
}
