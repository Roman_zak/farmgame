﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Farm_MAPZ
{
    internal class Invoker
    {
        private Point point;

        private ICommand _onStart;

        private ICommand _onFinish;

        public void SetOnStart(ICommand command)
        {
            this._onStart = command;
        }

        public void SetOnFinish(ICommand command)
        {
            this._onFinish = command;
        }
        public void DoSomethingImportant()
        {
            Economy economy = Economy.GetEconomy();
            int iter = 0;
            while (true)
            {
                point = this._onStart.execute(Point.Empty);
                List <Animal> checkList = economy.Animals.Where(animal => Math.Abs(animal.animalPic.Location.X-point.X)<=60|| 
                                                                          Math.Abs(animal.animalPic.Location.Y - point.Y) <= 80).ToList();
                if (checkList.Count==0 || economy.Animals.Count()==0 || iter>5) {
                    break;
                }
                iter++;
                Console.WriteLine("loop");
            }
            
            this._onFinish.execute(point);
        }
    }
}
