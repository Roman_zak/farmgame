﻿namespace Farm_MAPZ
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radioButtonHuman = new System.Windows.Forms.RadioButton();
            this.radioButtonElf = new System.Windows.Forms.RadioButton();
            this.radioButtonDworf = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.Transparent;
            this.buttonOk.BackgroundImage = global::Farm_MAPZ.Properties.Resources.OK;
            this.buttonOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOk.ForeColor = System.Drawing.Color.Transparent;
            this.buttonOk.Location = new System.Drawing.Point(699, 380);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(89, 58);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Farm_MAPZ.Properties.Resources.Thor;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(410, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(378, 308);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // radioButtonHuman
            // 
            this.radioButtonHuman.AutoSize = true;
            this.radioButtonHuman.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonHuman.Font = new System.Drawing.Font("Nirmala UI", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonHuman.ForeColor = System.Drawing.Color.Black;
            this.radioButtonHuman.Location = new System.Drawing.Point(90, 72);
            this.radioButtonHuman.MinimumSize = new System.Drawing.Size(200, 80);
            this.radioButtonHuman.Name = "radioButtonHuman";
            this.radioButtonHuman.Size = new System.Drawing.Size(236, 80);
            this.radioButtonHuman.TabIndex = 2;
            this.radioButtonHuman.TabStop = true;
            this.radioButtonHuman.Text = "Human";
            this.radioButtonHuman.UseVisualStyleBackColor = false;
            this.radioButtonHuman.CheckedChanged += new System.EventHandler(this.radioButtonHuman_CheckedChanged);
            // 
            // radioButtonElf
            // 
            this.radioButtonElf.AutoSize = true;
            this.radioButtonElf.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonElf.Font = new System.Drawing.Font("Nirmala UI", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonElf.ForeColor = System.Drawing.Color.Black;
            this.radioButtonElf.Location = new System.Drawing.Point(90, 173);
            this.radioButtonElf.MinimumSize = new System.Drawing.Size(200, 80);
            this.radioButtonElf.Name = "radioButtonElf";
            this.radioButtonElf.Size = new System.Drawing.Size(200, 80);
            this.radioButtonElf.TabIndex = 3;
            this.radioButtonElf.TabStop = true;
            this.radioButtonElf.Text = "Elf";
            this.radioButtonElf.UseVisualStyleBackColor = false;
            this.radioButtonElf.CheckedChanged += new System.EventHandler(this.radioButtonElf_CheckedChanged);
            // 
            // radioButtonDworf
            // 
            this.radioButtonDworf.AutoSize = true;
            this.radioButtonDworf.BackColor = System.Drawing.Color.Transparent;
            this.radioButtonDworf.Font = new System.Drawing.Font("Nirmala UI", 31.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDworf.ForeColor = System.Drawing.Color.Black;
            this.radioButtonDworf.Location = new System.Drawing.Point(90, 271);
            this.radioButtonDworf.MinimumSize = new System.Drawing.Size(200, 80);
            this.radioButtonDworf.Name = "radioButtonDworf";
            this.radioButtonDworf.Size = new System.Drawing.Size(210, 80);
            this.radioButtonDworf.TabIndex = 4;
            this.radioButtonDworf.TabStop = true;
            this.radioButtonDworf.Text = "Dworf";
            this.radioButtonDworf.UseVisualStyleBackColor = false;
            this.radioButtonDworf.CheckedChanged += new System.EventHandler(this.radioButtonDworf_CheckedChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Farm_MAPZ.Properties.Resources.settings;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.radioButtonDworf);
            this.Controls.Add(this.radioButtonElf);
            this.Controls.Add(this.radioButtonHuman);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonOk);
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton radioButtonHuman;
        private System.Windows.Forms.RadioButton radioButtonElf;
        private System.Windows.Forms.RadioButton radioButtonDworf;
    }
}