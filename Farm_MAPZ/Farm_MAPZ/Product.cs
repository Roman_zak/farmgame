﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
namespace Farm_MAPZ
{
    internal class Product :  Component, IClone //Clonnable
    {
        Random random = new Random();
        public uint Price { get; set; } 
        public uint Capacity { get; set; }
        public ProductType ProductType { get; }
        public PictureBox productPic;
        public Product(uint price, uint capacity, ProductType type)
        {
            this.Price = price;
            this.Capacity = capacity;
            this.ProductType = type;
      
        }
        public IClone clone()
        {
            
            Product copy = (Product)this.MemberwiseClone();
            
            copy.productPic = new PictureBox();
            switch (copy.ProductType)
            {
                case ProductType.Egg:
                    copy.productPic.Image = Properties.Resources.Eg; break;
                case ProductType.Wool:
                    copy.productPic.Image = Properties.Resources.Wool; break;
                case ProductType.Milk:
                    copy.productPic.Image = Properties.Resources.Milk; break;
                default: copy.productPic.Image = Properties.Resources.Eg; break;
            }
            copy.productPic.SizeMode = PictureBoxSizeMode.StretchImage;
            copy.productPic.Height = 80;
            copy.productPic.Width = 60;
            copy.productPic.BackColor = System.Drawing.Color.Transparent;

            /*            copy.productPic.Left = random.Next(10, Math.Abs(new GameScene().ClientSize.Width - copy.productPic.Width));
            */
            copy.productPic.Location = new Point(100, 100);
/*            copy.productPic.Top = random.Next(10, Math.Abs(new GameScene().ClientSize.Height - copy.productPic.Height));
*/           // Console.WriteLine(copy.productPic.Location);
            return copy;
        }
        /*        public override Clonnable clone()
                {
                    return (Product)this.MemberwiseClone();
                }*/

        public override string ToString()
        {
            return ProductType.ToString()+" price: "+Price.ToString()+", capacity: "+Capacity.ToString();
        }

        public override uint Operation()
        {
            return Price;
        }

        public override bool IsComposite()
        {
            return false;
        }
    }

    internal enum ProductType
    {
        Egg,
        Flour,
        Pancakes,
        Wool,
        Clew,
        Sweater,
        Milk,
        Cream,
        Cheese
    }
}

