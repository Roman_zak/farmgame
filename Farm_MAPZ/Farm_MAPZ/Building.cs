using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class Building : IClone//Clonnable
    {
        public uint Price { get; set; }
        public uint TaktTime { get; set; }
        public Product ProductIn { get; set; }
        public Product ProductOut { get; set; }
        public Building(uint price, uint taktTime, BuildingType buildingType, Product typeIn, Product typeOut)
        {
            this.Price = price;
            this.TaktTime = taktTime;
            this.ProductIn = typeIn;
            this.ProductOut = typeOut;
        }
        public IClone clone()
        {
            Building copy = (Building)this.MemberwiseClone();
            copy.ProductIn = (Product)this.ProductIn.clone();
            copy.ProductIn = (Product)this.ProductIn.clone();
            return copy;
        }
        public Product produce()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductIn.ProductType);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductIn.ProductType, out shelf))
            {
                shelf.Remove(ProductIn.ProductType);
                return (Product)ProductOut.clone();
            }
            else
            {
                return null;
            }
            //economy.Storage.Products.Remove(economy.Storage.Products.Where(i => i.ProductType == ProductIn.ProductType).First());
            
        }
    }

    internal enum BuildingType
    {
        EggFlour,
        FlourPancakes,
        WoolClew,
        ClewSweater,
        MilkCream,
        CreamCheese
    }
}