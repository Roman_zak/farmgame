﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Farm_MAPZ
{
    internal class Resiver
    {
        public Animal animal { get; set; }
        public Form mainForm;

        public Resiver(Animal animal, Form mainForm)
        {
            this.animal = animal;
            this.mainForm = mainForm;
        }

        public Point locate(Point point) {
            Economy economy = Economy.GetEconomy();
            
            Animal copy = (Animal)animal.clone();
            copy.animalPic.Location = point;
            economy.Animals.Add(copy);
            copy.makeProductive(mainForm);
            mainForm.Controls.Add(copy.animalPic);
            return point;
        }
    }
}
