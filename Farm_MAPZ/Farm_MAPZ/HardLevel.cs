using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    class LevelHardFactory : ILevelFactory
    {
        public Level CreateLevel()
        {
            uint ID = 0;
            string Name = "Hard Level";
            uint time = 15;//min
            uint money = 15000;


            Economy economy = Economy.GetEconomy();

            Storage _storage = new Storage(100);

            AnimalRepository ar = AnimalRepository.GetAnimalRepository();

            BuildingsRepository arB = BuildingsRepository.GetBuildingsRepository();

            economy.manufacturyContext = new ManufacturyContext(new MilkManufactury());
            //������������ Economic ��� HardLEvel

            economy.Storage = _storage;

            economy.Money = 110000;

            //economy.Buildings.Add((Building)arB.EggFlour.clone());
            ////////////////////////////

            //��������� �� ������������ Task ��� HardLEvel

            List<Animal> _animals = new List<Animal>();
            for (int i = 0; i < 7; i++)
            {
                _animals.Add((Animal)ar.Chicken.clone());
            }
            for (int i = 0; i < 2; i++)
            {
                _animals.Add((Animal)ar.Sheep.clone());
            }
            for (int i = 0; i < 1; i++)
            {
                _animals.Add((Animal)ar.Cow.clone());
            }

            LevelTask task = new LevelTask(money, _animals, _storage);
            ////////////////////////////

            Level normalLevel = new Level(Name, ID, time, task);

            return normalLevel;
        }
    }

}