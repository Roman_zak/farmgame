﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class AnimalRepository
    {
        private static AnimalRepository _animalRepository;
        private AnimalRepository()
        {
            Chicken = new Animal(state: new ChickenState(),animalType: AnimalType.Chicken, priceToBuy: 100, productToProduce: ProductRepository.GetProductRepository().Egg);
            Sheep = new Animal(state: new SheepState(), animalType: AnimalType.Sheep, priceToBuy: 1000, productToProduce: ProductRepository.GetProductRepository().Wool);
            Cow = new Animal(state: new CowState(), animalType: AnimalType.Cow, priceToBuy: 10000, productToProduce: ProductRepository.GetProductRepository().Milk);
        }
        public static AnimalRepository GetAnimalRepository()
        {
            if (_animalRepository == null)
            {
                _animalRepository = new AnimalRepository();
            }
            return _animalRepository;
        }
        public Animal Chicken { get; set; }
        public Animal Sheep { get; set; }
        public Animal Cow { get; set; }
    }
}