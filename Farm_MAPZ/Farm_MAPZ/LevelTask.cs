using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Farm_MAPZ
{

    internal sealed class LevelTask
    {
        public uint Money { get; set; }
        public List<Animal> animals { get; set; }
        public Storage storage { get; set; }
        public List<Building> buildings{get;set; }
        public LevelTask() { }
        public LevelTask(uint money, List<Animal> _animals, Storage _storage)
        {
            this.Money = money;
            this.animals = _animals;
            this.storage = _storage;
        }
    }
}
