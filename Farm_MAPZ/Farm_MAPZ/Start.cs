﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_MAPZ
{
    public partial class Start : Form
    {
        public Start()
        {
            InitializeComponent();
        }

        private void buttonLevelEz_Click(object sender, EventArgs e)
        {
            ILevelFactory levelFactory = new LevelEzFactory();
            
            LevelFactoryProxy levelFactoryProxy = new LevelFactoryProxy(levelFactory, Form1.Role);
            Level level1 = levelFactoryProxy.CreateLevel();
            GameScene scene = new GameScene();
            scene.Show();
            this.Close();
        }

        private void buttonLevelNormal_Click(object sender, EventArgs e)
        {
            ILevelFactory levelFactory = new LevelNormalFactory();
            LevelFactoryProxy levelFactoryProxy = new LevelFactoryProxy(levelFactory, Form1.Role);
            Level level1 = levelFactoryProxy.CreateLevel();
            GameScene scene = new GameScene();
            scene.Show();
            this.Close();
        }

        private void buttonLevelHard_Click(object sender, EventArgs e)
        {
            ILevelFactory levelFactory = new LevelHardFactory();
            LevelFactoryProxy levelFactoryProxy = new LevelFactoryProxy(levelFactory, Form1.Role);
            Level level1 = levelFactoryProxy.CreateLevel();
            GameScene scene = new GameScene();
            scene.Show();
            this.Close();
        }
    }
}
