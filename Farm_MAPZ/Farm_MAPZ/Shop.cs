﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_MAPZ
{
    public partial class Shop : Form
    {
        public Shop()
        {
            InitializeComponent();
        }
        private Form mainForm;
        public Shop(Form _mainForm)
        {
            mainForm = _mainForm;
            InitializeComponent();
        }
        int ChickenCount()
        {
            Economy economy = Economy.GetEconomy();
            return economy.Animals.Count(animal => animal.AnimalType == AnimalType.Chicken);
        }

        int SheepCount()
        {
            Economy economy = Economy.GetEconomy();
            return economy.Animals.Count(animal => animal.AnimalType == AnimalType.Sheep);
        }

        int CowCount()
        {
            Economy economy = Economy.GetEconomy();
            return economy.Animals.Count(animal => animal.AnimalType == AnimalType.Cow);
        }

        int EggCount()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductType.Egg);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductType.Egg, out shelf))
            {
                return shelf.getChildrens().Count;
            }
            else
            {
                return 0;
            }
        }

        int WoolCount()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductType.Wool);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductType.Wool, out shelf))
            {
                return shelf.getChildrens().Count;
            }
            else
            {
                return 0;
            }
        }

        int MilkCount()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductType.Milk);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductType.Milk, out shelf))
            {
                return shelf.getChildrens().Count;
            }
            else
            {
                return 0;
            }
        }
        int FlourCount()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductType.Flour);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductType.Flour, out shelf))
            {
                return shelf.getChildrens().Count;
            }
            else
            {
                return 0;
            }
        }
        int ClewCount()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductType.Clew);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductType.Clew, out shelf))
            {
                return shelf.getChildrens().Count;
            }
            else
            {
                return 0;
            }
        }
        int CreamCount()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductType.Cream);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductType.Cream, out shelf))
            {
                return shelf.getChildrens().Count;
            }
            else
            {
                return 0;
            }
        }
        public void UpdateShop() {
            Economy economy = Economy.GetEconomy();
            this.labelChickenCount.Text = ChickenCount().ToString();
            this.labelSheepCount.Text = SheepCount().ToString();
            this.labelCowCount.Text = CowCount().ToString();
            this.labelEggCount.Text = EggCount().ToString();
            this.labelWoolCount.Text = WoolCount().ToString();
            this.labelMilkCount.Text = MilkCount().ToString();
            this.label_StorageWorth.Text = "Sorage worth: " + economy.Storage.storageWorth();
            this.label_Cream.Text = CreamCount().ToString();
            this.label_Nytka.Text = ClewCount().ToString();
            this.label_Eggs.Text = FlourCount().ToString();
        }
        private void buttonAddChicken_Click(object sender, EventArgs e)
        {

            Economy economy = Economy.GetEconomy();
            if(economy.Money< AnimalRepository.GetAnimalRepository().Chicken.PriceToBuy)
            {

            }
            else
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleLocationCommand());
                invoker.SetOnFinish(new ComplexLocationCommand(Point.Empty, mainForm, AnimalRepository.GetAnimalRepository().Chicken));
                invoker.DoSomethingImportant();
                /*              economy.Money-= AnimalRepository.GetAnimalRepository().Chicken.PriceToBuy;
                                Animal newChicken = (Animal)AnimalRepository.GetAnimalRepository().Chicken.clone();
                                economy.Animals.Add(newChicken);
                                newChicken.makeProductive(mainForm);
                                mainForm.Controls.Add(newChicken.animalPic);
                                Console.WriteLine("newChicken");*/
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void buttonRemoveChicken_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            Animal animalToRemove = economy.Animals.Find(a => a.AnimalType == AnimalType.Chicken);
            if(animalToRemove != null)
            {
                ((GameScene)mainForm).Controls.Remove(animalToRemove.animalPic);
                economy.Money += animalToRemove.PriceToBuy / 2;
                economy.Animals.Remove(animalToRemove);
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
            //
        }

        private void buttonAddSheep_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            if (economy.Money < AnimalRepository.GetAnimalRepository().Sheep.PriceToBuy)
            {

            }
            else
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleLocationCommand());
                invoker.SetOnFinish(new ComplexLocationCommand(Point.Empty, mainForm, AnimalRepository.GetAnimalRepository().Sheep));
                invoker.DoSomethingImportant();
                /*                economy.Money -= AnimalRepository.GetAnimalRepository().Sheep.PriceToBuy;
                                Animal newAnimal = (Animal)AnimalRepository.GetAnimalRepository().Sheep.clone();
                                economy.Animals.Add(newAnimal);
                                newAnimal.makeProductive(mainForm);
                                mainForm.Controls.Add(newAnimal.animalPic);*/
                Console.WriteLine("newSheep");
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void buttonRemoveSheep_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            Animal animalToRemove = economy.Animals.Find(a => a.AnimalType == AnimalType.Sheep);
            if (animalToRemove != null)
            {
                ((GameScene)mainForm).Controls.Remove(animalToRemove.animalPic);
                economy.Money += animalToRemove.PriceToBuy / 2;
                economy.Animals.Remove(animalToRemove);
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void buttonAddCow_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            if (economy.Money < AnimalRepository.GetAnimalRepository().Cow.PriceToBuy)
            {

            }
            else
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleLocationCommand());
                invoker.SetOnFinish(new ComplexLocationCommand(Point.Empty, mainForm, AnimalRepository.GetAnimalRepository().Cow));
                invoker.DoSomethingImportant();
                /*                economy.Money -= AnimalRepository.GetAnimalRepository().Cow.PriceToBuy;
                                Animal newAnimal = (Animal)AnimalRepository.GetAnimalRepository().Cow.clone();
                                newAnimal.makeProductive(mainForm);
                                economy.Animals.Add(newAnimal);
                                mainForm.Controls.Add(newAnimal.animalPic);*/
                Console.WriteLine("newCow");
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void buttonRemoveCow_Click(object sender, EventArgs e)
        {
            Economy economy = Economy.GetEconomy();
            Animal animalToRemove = economy.Animals.Find(a => a.AnimalType == AnimalType.Cow);
            if (animalToRemove != null)
            {
                ((GameScene)mainForm).Controls.Remove(animalToRemove.animalPic);
                economy.Money += animalToRemove.PriceToBuy / 2;
                economy.Animals.Remove(animalToRemove);
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void labelEgg_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Egg");
            if (Economy.GetEconomy().Storage.removeProduct(ProductType.Egg))
            {
                Economy.GetEconomy().Money += ProductRepository.GetProductRepository().Egg.Price;
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }

        }

        private void labelWool_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Wool");
            if (Economy.GetEconomy().Storage.removeProduct(ProductType.Wool))
            {
                Economy.GetEconomy().Money += ProductRepository.GetProductRepository().Wool.Price;
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void labelMilk_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Milk");
            if (Economy.GetEconomy().Storage.removeProduct(ProductType.Milk))
            {
                Economy.GetEconomy().Money += ProductRepository.GetProductRepository().Milk.Price;
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void Shop_Load(object sender, EventArgs e)
        {
            FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
            facade.Update();
        }

        private void labelEggCount_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label_FlourSell_Click(object sender, EventArgs e)
        {
            if (Economy.GetEconomy().Storage.removeProduct(ProductType.Flour))
            {
                Economy.GetEconomy().Money += ProductRepository.GetProductRepository().Flour.Price;
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void label_NytkaSell_Click(object sender, EventArgs e)
        {
            if (Economy.GetEconomy().Storage.removeProduct(ProductType.Clew))
            {
                Economy.GetEconomy().Money += ProductRepository.GetProductRepository().Clew.Price;
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }

        private void label_CreamSell_Click(object sender, EventArgs e)
        {
            if (Economy.GetEconomy().Storage.removeProduct(ProductType.Cream))
            {
                Economy.GetEconomy().Money += ProductRepository.GetProductRepository().Cream.Price;
                FacadeUpdate facade = new FacadeUpdate((GameScene)mainForm, this);
                facade.Update();
            }
        }
    }
}
