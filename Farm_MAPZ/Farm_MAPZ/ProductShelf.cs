﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class ProductShelf : Component
    {
        public ProductType ProductType;

        public ProductShelf(ProductType type)
        {
            ProductType = type;
        }
        protected List<Component> _children = new List<Component>();

        public List<Component> getChildrens()
        {
            return _children;
        }
        public override void Add(Component component)
        {
            this._children.Add(component);
        }

        public override void Remove(Component component)
        {
            this._children.Remove(component);
        }
        public void Remove(ProductType type)
        {
            if (this._children.Count > 0)
            {
                this._children.Remove(_children.ElementAt(0));
            }
            
        }
        // The Composite executes its primary logic in a particular way. It
        // traverses recursively through all its children, collecting and
        // summing their results. Since the composite's children pass these
        // calls to their children and so forth, the whole object tree is
        // traversed as a result.
        public override uint Operation()
        {
            uint result = 0;

            foreach (Component component in this._children)
            {
                result += component.Operation();
            }

            return result;
        }
    }
}
