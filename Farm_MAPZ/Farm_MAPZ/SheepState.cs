﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class SheepState : State
    {
        public override void HandleProduce()
        {
            Console.WriteLine("Sheep produce");
            SoundPlayer simpleSound = new SoundPlayer("Sheep.wav");
            simpleSound.Play();
        }

    }
}
