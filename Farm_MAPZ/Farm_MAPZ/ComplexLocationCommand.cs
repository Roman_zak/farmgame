﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
namespace Farm_MAPZ
{
    internal class ComplexLocationCommand : ICommand
    {
        public Resiver _resiver;
        Point _location;
        Form mainForm;
        Animal animal;

        public ComplexLocationCommand(Point location, Form mainForm, Animal animal)
        {
            _location = location;
            this.mainForm = mainForm;
            this.animal = animal;
        }

        public Point execute(Point point)
        {
            _resiver = new Resiver(animal, mainForm);
            return _resiver.locate(point);
        }
    }
}
