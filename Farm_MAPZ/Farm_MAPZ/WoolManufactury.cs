﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Farm_MAPZ
{
    internal class WoolManufactury : IManufacturyStrategy
    {
        public Product ProductIn = ProductRepository.GetProductRepository().Wool;
        public Product ProductOut = ProductRepository.GetProductRepository().Clew;
        private PictureBox manufactPic = new PictureBox();
        public WoolManufactury()
        {
            manufactPic.Image = Properties.Resources.SheepFarm;
        }
        public PictureBox GetPictureBox()
        {
            return manufactPic;
        }
        public Product manufacture()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductIn.ProductType);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductIn.ProductType, out shelf))
            {
                shelf.Remove(ProductIn.ProductType);
                return (Product)ProductOut.clone();
            }
            else
            {
                return null;
            }
        }
    }
}
