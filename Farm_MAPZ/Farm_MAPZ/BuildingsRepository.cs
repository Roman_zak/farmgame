using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Farm_MAPZ
{
    internal class BuildingsRepository
    {
        private static BuildingsRepository _buildingsRepository;
        private BuildingsRepository()
        {
            EggFlour = new Building(price: 400, taktTime: 5, buildingType: BuildingType.EggFlour, typeIn: ProductRepository.GetProductRepository().Egg, typeOut: ProductRepository.GetProductRepository().Flour);
            FlourPancakes = new Building(price: 600, taktTime: 5, buildingType: BuildingType.FlourPancakes, typeIn: ProductRepository.GetProductRepository().Flour, typeOut: ProductRepository.GetProductRepository().Pancakes);
            WoolClew = new Building(price: 2000, taktTime: 5, buildingType: BuildingType.WoolClew, typeIn: ProductRepository.GetProductRepository().Wool, typeOut: ProductRepository.GetProductRepository().Clew);
            ClewSweater = new Building(price: 6000, taktTime: 5, buildingType: BuildingType.ClewSweater, typeIn: ProductRepository.GetProductRepository().Clew, typeOut: ProductRepository.GetProductRepository().Sweater);
            MilkCream = new Building(price: 4000, taktTime: 5, buildingType: BuildingType.MilkCream, typeIn: ProductRepository.GetProductRepository().Milk, typeOut: ProductRepository.GetProductRepository().Cream);
            CreamCheese = new Building(price: 6000, taktTime: 5, buildingType: BuildingType.CreamCheese, typeIn: ProductRepository.GetProductRepository().Cream, typeOut: ProductRepository.GetProductRepository().Cheese);
        }
        public static BuildingsRepository GetBuildingsRepository()
        {
            if (_buildingsRepository == null)
            {
                _buildingsRepository = new BuildingsRepository();
            }
            return _buildingsRepository;
        }
        public Building EggFlour { get; set; }
        public Building FlourPancakes { get; set; }
        public Building WoolClew { get; set; }
        public Building ClewSweater { get; set; }
        public Building MilkCream { get; set; }
        public Building CreamCheese { get; set; }
    }

}