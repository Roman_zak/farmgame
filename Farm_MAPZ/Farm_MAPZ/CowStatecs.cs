﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_MAPZ
{
    internal class CowState : State
    {
        bool _isLeft = true;
        public override void HandleProduce()
        {
            Console.WriteLine("Cow produce");
            if (_isLeft)
            {
                this._context.animalPic.Image = Properties.Resources.CowImage_down;
                _isLeft = false;
            }
            else
            {
                this._context.animalPic.Image = Properties.Resources.CowImage;
                _isLeft = true;
            }

        }

    }
}
