﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Farm_MAPZ
{
    internal class EggManufactury : IManufacturyStrategy
    {
        public Product ProductIn = ProductRepository.GetProductRepository().Egg;
        public Product ProductOut = ProductRepository.GetProductRepository().Flour;
        private PictureBox manufactPic = new PictureBox();
        public EggManufactury()
        {
            manufactPic.Image = Properties.Resources.ChickenFarm;
        }

        public PictureBox GetPictureBox()
        {
            return manufactPic;
        }

        public Product manufacture()
        {
            Economy economy = Economy.GetEconomy();
            ProductShelf shelf = new ProductShelf(ProductIn.ProductType);
            if (economy.Storage.ProductShelfs.TryGetValue(ProductIn.ProductType, out shelf))
            {
                shelf.Remove(ProductIn.ProductType);
                return (Product)ProductOut.clone();
            }
            else
            {
                return null;
            }
        }
    }
}
