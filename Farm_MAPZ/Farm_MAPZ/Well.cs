﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Farm_MAPZ
{
    internal class Well
    {
        private static Well _well;

        public static uint grass;

        public static Well GetWell()
        {
            if (_well == null)
            {
                _well = new Well();
            }
            return _well;
        }
        public  uint Volume { get; set; }
        public  bool pullWater()
        {
            Economy economy = Economy.GetEconomy();
            if (Volume == 10)
            {
                Console.WriteLine("Your watter is full");
                return false;
            }
            else if (economy.Money < 5)
            {
                Console.WriteLine("You dont have enough money");
                return false;
            }
            else
            {
                economy.Money -= 5;
                switch (Volume)
                {
                    case 9:
                        Volume += 1;
                        break;
                    case 8:
                        Volume += 2;
                        break;
                    case 7:
                        Volume += 3;
                        break;
                    case 6:
                        Volume += 4;
                        break;
                    default:
                        Volume += 5;
                        break;
                }
                return true;

            }

        }
        /*public static void Water()
        {
            this.Volume--;
            grass++;
        }*/
    }
}